### Manual de instalacion

# Aplicacion para consultar el clima de una ciudad

# Prerequisitos
> Tener instalado:  nodejs


# Pasos para instalar en un ambiente local

## Instrucciones

- Clonar el repositorio

```
git clone https://gitlab.com/javier.mesias.everis/wheather.git
```

- Entrar al directorio de nombre client y ejecutar el siguiente comando para instalar las depencias necesarias para poder levantar el front:

```
npm install
```

- Entrar al directorio de nombre server y ejecutar el siguiente comando para instalar las dependencias necesarias para poder levantar backend:

```
npm install
``` 

- Dentro del directorio server ejecutar el siguiente comando para levantar el api rest : 

```
node server.js
``` 

Esto levantara el api rest en el puerto 8000 [http://localhost:8000/v1](http://localhost:8000/v1). 


- Dentro del directorio client ejecutamos: 

```
npm start
``` 

Esto levantara el front en el puerto 3000 [http://localhost:3000](http://localhost:3000). 


## Fin instalación local



## Instrucciones para correr los test

- Entrar al directorio  server y ejecutar el comando:

```
npm install -g mocha
``` 

- Entrar al directorio  server/tests y ejecutar el comando:

```
mocha
``` 

## Captura

![alt text](https://gitlab.com/javier.mesias.everis/wheather/raw/master/desktop.png)