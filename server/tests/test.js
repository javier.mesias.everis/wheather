var assert = require('assert');
var request = require('supertest')
var app = require('../app.js')

 var request = request("http://localhost:8000")

 describe('/v1/current/', function() {
     describe('GET', function(){
         it('Should return current wheather json as default data format', function(done){
             request.get('/v1/current/')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
     });
 });


  describe('/v1/current/Buenos+Aires', function() {
     describe('GET', function(){
         it('Should return current wheather json as default data format', function(done){
             request.get('/v1/current/Buenos+Aires')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
     });
 });

    describe('/v1/current/nanana', function() {
     describe('GET', function(){
         it('Should return json with error as default data format and code 404 not found', function(done){
             request.get('/v1/current/nanana')
                 .expect('Content-Type', /json/)
                 .expect(404, done);
         });
     });
 });


  describe('/v1/forecast/', function() {
     describe('GET', function(){
         it('Should return forecast json as default data format', function(done){
             request.get('/v1/current/')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
     });
 });


  describe('/v1/forecast/Buenos+Aires', function() {
     describe('GET', function(){
         it('Should return forecast json as default data format', function(done){
             request.get('/v1/current/Buenos+Aires')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
     });
 });

    describe('/v1/forecast/nanana', function() {
     describe('GET', function(){
         it('Should return forecast json with erro as default data format and code 404 not found', function(done){
             request.get('/v1/current/nanana')
                 .expect('Content-Type', /json/)
                 .expect(404, done);
         });
     });
 });