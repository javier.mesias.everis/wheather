var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var helper = require('./Functions');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', function (req, res) {
    res.send('Api de servicio de clima');
});

router.get('/location',  function (req, response) {
   
		helper.getCurrentCity().then(function(data) {
			   response.setHeader('Access-Control-Allow-Origin', '*');
			   response.status(200).send(data);
		 }).catch(function(err){
		 	    response.setHeader('Access-Control-Allow-Origin', '*');
		    	response.status(404).send(err);
		 });

});

router.get('/current/:city?', function (req, response) {
    
		let city = req.params.city;
	    if (!city) {
	    	helper.getCurrentCity().then(function(data) {
				   return city = data.data.city;
			 }).then(function(name){ 
			 		helper.loadWheather(name,response);
			 }).catch(function(err){
			 		response.setHeader('Access-Control-Allow-Origin', '*');
					return response.status(404).send(err);
			 });
	    }else{
	    	helper.loadWheather(city,response);
	    }	
});


router.get('/forecast/:city?', function (req, response) {
    
    	let city = req.params.city;
	    if (!city) {
	    	helper.getCurrentCity().then(function(data) {
				   return city = data.data.city;
			 }).then(function(name){ 
			 		helper.loadForecast(name,response);
			 }).catch(function(err){
			 		response.setHeader('Access-Control-Allow-Origin', '*');
					return response.status(500).send(err);
			 });
	    }else{
	    	helper.loadForecast(city,response);
	    }	

});


module.exports = router;