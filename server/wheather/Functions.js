var constants = require('./Constants');
const request = require('request');

module.exports = {
	prepareCity : (data) => {
		const  { city, country , countryCode, lat, lon, regionName ,status, zip  } = data;
		return { city, country , countryCode, lat, lon, regionName ,status, zip };
	},
	prepareWheather : (data) => {
		if(data.cod !== 200 ) return;
		let country = ( typeof  data.sys !== 'undefined') ?  data.sys.country : null
		const  {  name, coord, weather , main, visibility, id, code  } = data;
		let icon;
		data.weather.map((w) => { icon = w.icon })

		return {  country,  name, coord, weather , main, visibility, id, code , icon:constants.WHEATHER_API_ICON + icon + '.png' };
		//return data;
	},
	getForecasts : (data) => {
		
	},
	prepareForecast : (data) => {
		if(data.cod !== '200' ) return;
		const  { list  } = data;
		let resp =new Object() ;
		let date1;
		resp["city"] = data.city;
		resp["list"] =[] ;
		resp["cnt"] = data.cnt;
		let cont2 = 0;
		data.list.map((w,k,arr) => { 
				let index = w.dt_txt.substring(0,10).replace(/-/g, '');
				
				if (date1 != index){
						let objAux =new Object();
						resp["list"][cont2] = new Object();
						resp["list"][cont2][index] = [];
						let cont = 0;
						arr.map((arrAux,ind)=>{
								let indAux = arrAux.dt_txt.substring(0,10).replace(/-/g, '');
								if (indAux === index ){
									objAux[cont++] = arrAux;
								}
						})
						
						resp["list"][cont2][index] = objAux;
						cont2++;
				}
				date1 = index;
		})

	
		return resp ;
	},
	getCurrentCity : () => {
		return new Promise((resolve, reject)=>{
					request(constants.IP_API_URL, { json: true }, (err, res, body) => {
						if (err) {
							 let objCityErr = {data:{Error:constants.ERROR_MESSAGE},status:'0'}
							 reject(objCityErr)
						}  
						else {
								resolve({data:module.exports.prepareCity(body),status:'200'})
							}

						});
				}); 
	},
	getWheatherCity : (city) => {
		
		let URL_WEATHER = constants.WHEATHER_API_URL + 'weather?q='+city+'&lang=es&units=metric&APPID=' + constants.WHEATHER_API_KEY
		
		return new Promise((resolve, reject)=>{
					request(URL_WEATHER, { json: true }, (err, res, body) => {
						if (err) {
							 let objCityErr = {data:{Error:constants.ERROR_MESSAGE},status:'0'}
							 reject(objCityErr)
						}  
						else  {
							//console.log(body)
								if (parseInt(body.cod) !== 200) {reject(body)} else 
								{resolve({data:module.exports.prepareWheather(body),status:'200'})}
							}

						});
				})

	},
	getForecastCity : (city) => {
		
		let URL_FORECAST = constants.WHEATHER_API_URL + 'forecast?q='+city+'&lang=es&units=metric&APPID=' + constants.WHEATHER_API_KEY
		//console.log(URL_FORECAST);
		return new Promise((resolve, reject)=>{
					request(URL_FORECAST, { json: true }, (err, res, body) => {
						if (err) {
							 let objErr = {data:{Error:constants.ERROR_MESSAGE},status:'0'}
							 reject(objErr)
						}  
						else  {
									if (parseInt(body.cod) !== 200) {reject(body)} else 
									{resolve( {data:module.exports.prepareForecast(body),status:'200'} )}

							   }

						});
				})

	},

	loadWheather : (city,response) => {
		module.exports.getWheatherCity(city).then(function(data) {
					response.setHeader('Access-Control-Allow-Origin', '*');
					response.status(200).send(data);
				}).catch(function(err){
					response.setHeader('Access-Control-Allow-Origin', '*');
					response.status(404).send(err);
				});
	},

	loadForecast : (city,response) => {
		module.exports.getForecastCity(city).then(function(data) {
					response.setHeader('Access-Control-Allow-Origin', '*');
					response.status(200).send(data);
				}).catch(function(err){
					response.setHeader('Access-Control-Allow-Origin', '*');
					response.status(404).send(err);
				});
	}
};