import constants from "../Constants";

const initialState = {
            localCity:null,
            wheatherCity:null,
            wheatherForecast:null
}

const mainReducer=(state=initialState,action)=>{

    if(action.type === constants.LOAD_CURRENT_CITY){
        return{
            ...state,
            localCity:action.data.data.data,
            wheatherCity:{...state.wheatherCity},
            wheatherForecast:null,
            error_city:0
        }
    }

     if (action.type === constants.LOAD_CURRENT_WHEATHER) { 
         return{
            ...state,
            localCity: {...state.localCity},
            wheatherCity:action.data.data.data,
            wheatherForecast:{...state.wheatherForecast},
            city:action.city,
            error_city:0
        }
    }

    if (action.type === constants.LOAD_FORECAST_WHEATHER) { 
        
         return{
            ...state,
            localCity: {...state.localCity},
            wheatherCity: {...state.wheatherCity},
            wheatherForecast:action.data.data.data,
            city:action.city,
            error_city:0
        }
    }  

    if (action.type === constants.LOAD_CITY_NOT_FOUND) { 
         return{
            localCity:{...state.localCity},
            wheatherCity:null,
            wheatherForecast:null,
            error_city:1
        }
    }else{
        return{
            ...state,
            initialState
        }
    }


}

export default mainReducer;