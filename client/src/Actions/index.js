import axios from "axios";
import constants from "../Constants";

export function loadCurrentCity(){
    return(dispatch)=>{

        return axios.get(constants.URL_LOAD_CURRENT_CITY).then((response)=>{
            dispatch(setCurrentCity(response));
        }).catch(error => {
		    //console.log(error)
		});
    }
}


export function loadCurrentWheater(city){
	let url = constants.URL_CURRENT_WHEATHER;
	url += (typeof city === "undefined" ) ? '' : city;
    return(dispatch)=>{
        return axios.get(url).then((response)=>{
            dispatch(setCurrentWheather(response,city));
        }).catch(error => {
		      dispatch(setCityNotFound(error));
		});
    }
}

export function loadForecastWheater(city){
	let url = constants.URL_FORECAST_WHEATHER;
	url += (typeof city === "undefined" ) ? '' : city;
    return(dispatch)=>{
        return axios.get(url).then((response)=>{
            dispatch(setForecastWheather(response,city));
        }).catch(error => {
		    console.log(error)
		});
    }
}

export function setCurrentCity(data){
	
    return{
        type:constants.LOAD_CURRENT_CITY,
        data
    }
}

export function setCurrentWheather(data,city){
	
    return{
        type:constants.LOAD_CURRENT_WHEATHER,
        data,
        city
    }
}


export function setForecastWheather(data,city){
	
    return{
        type:constants.LOAD_FORECAST_WHEATHER,
        data,
        city
    }
}

export function setCityNotFound(data){
    
    return{
        type:constants.LOAD_CITY_NOT_FOUND,
        data
    }
}