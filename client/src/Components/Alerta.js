import React from 'react';

const Alerta = ({ error }) => {
		return (
			<div className="alert alert-warning" role="alert">
				<h1 className="alert-heading">Opps!  <i className="fa fa-cloud	"></i> </h1>
				<p>Esta ciudad no esta disponible intente con otra.</p>
			</div>
		);
}

export default Alerta;