import React , {  Fragment  } from 'react';
import moment from 'moment';
import ForecastItem from './ForecastItem';

const ForecastRow = ({ dates} ) => {
		//console.log(  dates );

		return (

			<Fragment>
				
					{

						Object.keys(dates).map(function(key) {
							moment.locale('es');
						  	return(
						  			<Fragment key={key}>

										<div className="w-100">
											<span className="badge badge-info mt-2 mb-2">{ moment(key).format('MMM DD, YYYY')  }</span>
										
										</div>
										<div className="w-100 ">
											{

													Object.keys(dates[key]).map(function(k) {
													  	return(
													  			<Fragment key={k}>
																	<ForecastItem  item={dates[key][k]} />
													  			</Fragment>
													  	)
													  
													})
											}
										</div>

						  			</Fragment>
						  	)
						  
						})

					}	
						
				
			</Fragment>
		);
}

export default ForecastRow;