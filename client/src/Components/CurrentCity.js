import React  from 'react';

const CurrentCity = ({ localCity }) => {
		
		return (
				
	                <p className="text-light " >
	                		<span className="mr-1">Ciudad Local</span>
	                		<b>{localCity.regionName}, {localCity.country}</b>
	                </p>
               
		);
}

export default CurrentCity;