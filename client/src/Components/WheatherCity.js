import React  from 'react';

const WheatherCity = ({ wheatherCity }) => {
 
    const color = () =>{  return {color:'#2F3740'}  }
    return (
       
            <div className="pl-2 border border-dark rounded shadow ">
              <h1 style={color()}>{wheatherCity.name} , {wheatherCity.country}  ({wheatherCity.main.temp}°)</h1>
              <img src={wheatherCity.icon} alt={wheatherCity.weather[0].description} />
              <p className="font-italic mr-5 h5 text-capitalize " style={color()} >{wheatherCity.weather[0].description}</p>
              <p className="font-normal mr-5 h5 " style={color()}><b>Humedad:</b> {wheatherCity.main.humidity} % </p>
              <p className="font-normal mr-5 h5 " style={color()}><i className="fa fa-thermometer-empty mr-2"></i><b>Min:</b> {wheatherCity.main.temp_min}° </p>
              <p className="font-normal mr-5 h5 " style={color()}><i className="fa fa-thermometer-full mr-2"></i><b>Max:</b> {wheatherCity.main.temp_max}° </p>
            </div>
            
    );
}

export default WheatherCity;