import React , { Component , Fragment}  from 'react';



class SearchCities extends Component {
    
    state = { texto:'' }    

    constructor (props) {
        super(props)
       
    }
    
     loadWheatherByCity = (e) => {

            let city = document.getElementById('search-city').value;
            this.props.loadCurrentWheater(city)
            this.props.loadForecastWheater(city)
    }

    handleChange = e => {
        this.setState({texto: e.target.value})
    }

      render() {
            //const { loadCurrentWheater , loadForecastWheater } = this.props;
                return (
                  
                    <Fragment>
                    <div className="input-group mb-3">
                     <input 
                            id="search-city"
                            className="form-control mr-1 shadow " 
                            placeholder="Ingrese una ciudad.. Eje: Buenos Aires"
                            value={this.state.texto} onChange={this.handleChange}
                        />
                        <span className="input-group-btn">
                        <button type="button" onClick={ (e) => { this.loadWheatherByCity(e)  } } disabled={!this.state.texto} className="btn btn-info"><i className="fa fa-search"></i></button>
                      </span>
                    </div>
                    </Fragment>

                )
    }

    
}

export default SearchCities;