import React  from 'react';



const SelectCities = (props) => {
    const { loadCurrentWheater , loadForecastWheater } = props;
    
    const loadWheatherByCity = (city) => {
            loadCurrentWheater(city)
            loadForecastWheater(city)
    }

    return (
                  <select onChange={ (e) => { loadWheatherByCity(e.target.value)  } }  className="shadow  form-control">
                        <option value="Buenos Aires, Ar">Seleccione una ciudad..</option>
                       <option value="Buenos Aires, Ar">Buenos Aires</option>
                        <option value="Catamarca, Ar">Catamarca</option>
                        <option value="Chaco, Ar">Chaco</option>
                        <option value="Chubut, Ar">Chubut</option>
                        <option value="Cordoba, Ar">Cordoba</option>
                        <option value="Corrientes, Ar">Corrientes</option>
                        <option value="Entre Rios, Ar">Entre Rios</option>
                        <option value="Formosa, Ar">Formosa</option>
                        <option value="Jujuy, Ar">Jujuy</option>
                        <option value="La Pampa, Ar">La Pampa</option>
                        <option value="La Rioja, Ar">La Rioja</option>
                        <option value="Mendoza, Ar">Mendoza</option>
                        <option value="Misiones, Ar">Misiones</option>
                        <option value="Neuquen, Ar">Neuquen</option>
                        <option value="Rio Negro, Ar">Rio Negro</option>
                        <option value="Salta, Ar">Salta</option>
                        <option value="San Juan, Ar">San Juan</option>
                        <option value="San Luis, Ar">San Luis</option>
                        <option value="Santa Cruz, Ar">Santa Cruz</option>
                        <option value="Santa Fe, Ar">Santa Fe</option>
                        <option value="Sgo. del Estero, Ar">Sgo. del Estero</option>
                        <option value="Tierra del Fuego, Ar">Tierra del Fuego</option>
                       <option value="Tucuman, Ar">Tucuman</option>
                  </select>
            
    );
}

export default SelectCities;