import React , {Component , Fragment } from 'react';
import {connect} from "react-redux";
import {loadCurrentCity , loadCurrentWheater, loadForecastWheater} from "../../Actions/index.js"

import CurrentCity from '../CurrentCity';
import SelectCities from './SelectCities';
import SearchCities from './SearchCities';

class Header  extends Component {
        
      constructor (props) {
        super(props)
        this.props.loadCurrentCity()
        //console.log(this.props)
      }

      handleSelectedCity = () => {
         this.props.loadCurrentWheater('Roma')
         this.props.loadForecastWheater('Roma')
      }


        render() {
            
            return (
                <Fragment>
                  <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between d-flex mb-4">
                      <div className="container">
                        
                          <p className="text-light h3 mr-2 w-100"><i className="fa fa-cloud mr-2"></i><b>Wheather</b></p>
                           { this.props.localCity ?  <CurrentCity localCity={this.props.localCity}  /> : ''   }
                           <div className="ml-4">
                           
                            </div>
                          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navegacion" aria-controls="navegacion" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="navbar-toggler-icon"></span>
                          </button>

                          <div className="collapse navbar-collapse" id="navegacion">

                              <ul className="navbar-nav ml-auto text-right">
                                  <li className="nav-item dropdown mr-md-2 mb-2 mb-md-0">
                                          <div className="" aria-labelledby="navegacion">
                                          </div>
                                  </li>
                                  <li className="nav-item dropdown mr-md-2 mb-2 mb-md-0" >
                                                 
                                  </li>

                              </ul>
                          </div>
                      </div>
                  </nav>
                  <div className="row">
                      <div className="col-md-6">
                          <div className="mb-3 ">
                                <SelectCities loadCurrentWheater={this.props.loadCurrentWheater} loadForecastWheater={ this.props.loadForecastWheater}  />
                          </div>
                      </div>
                       <div className="col-md-6">
                            <SearchCities loadCurrentWheater={this.props.loadCurrentWheater} loadForecastWheater={ this.props.loadForecastWheater}  />
                       </div>
                  </div>
                </Fragment>
            );
    }
}

const mapStateToProps=(state) =>{
    //console.log(state)
    return { localCity:state.localCity , wheatherCity:state.wheatherCity  }
};



export default connect (mapStateToProps, {loadCurrentCity,loadCurrentWheater,loadForecastWheater})(Header);