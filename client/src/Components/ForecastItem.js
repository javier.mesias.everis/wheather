import React  from 'react';
import moment from 'moment';
//import ForecastRow from './ForecastRow';
import constants from "../Constants";

const ForecastItem = ({ item} ) => {
		
		const color = () =>{  return {color:'#4D4E4F'}  }
		let icon = constants.WHEATHER_API_ICON + item.weather[0].icon + ".png";

		return (
			
				<div className=" rounded shadow-sm bg-light border border-dark  w-100 mr-2 mt-2 mb-2">
					<div className="">
						<img src={icon} alt={item.weather[0].description} /> 
						<span className="badge badge-success mr-2 ml-2 pb-1 "> <i className="fa fa-thermometer-empty"></i> {item.main.temp}°  /  <span className="text-light">Min: {item.main.temp_min}° -  Max: {item.main.temp_max}° </span> /  H: {item.main.humidity}% </span>
						<span className="font-italic mr-5 text-capitalize">{item.weather[0].description}</span>
						<span className="font-weight-bold float-right mr-2 mt-1" style={color()}> { moment(item.dt_txt).format('HH:mm')  }</span>				
					</div>
				</div>
			
		);
}

export default ForecastItem;