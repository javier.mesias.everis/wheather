import React , {  Fragment  } from 'react';
import ForecastRow from './ForecastRow';

const Forecast = ({ forecastCity} ) => {
		//console.log(forecastCity);
		return (
			<Fragment>
			<div className="col-md-12">
				
				<div className="row pl-2 pr-2 mt-3  border border-dark rounded shadow ">
				<h3 className="mt-2 mb-2">
						 Pronóstico Extendido,  {forecastCity.city.name }
					</h3>
						{
							forecastCity.list.map((dates,i) => {

									return (
											<Fragment key={i} >
													<ForecastRow dates={dates} />
											</Fragment>
									)	
							})
						}
						
				</div>
				</div>
			</Fragment>
		);
}

export default Forecast;