import React , {Component  } from 'react';
import {connect} from "react-redux";
import {loadCurrentWheater } from "../Actions/index.js"
import WheatherCity from './WheatherCity';
import Alerta from './Alerta';

class CurrentWheatherCity  extends Component {
       
     
      	constructor (props) {
        super(props)
      	this.props.loadCurrentWheater()
      	
      }


        render() {
        	
            return (
             
               <div className="row">
               		<div className="col-md-12">
               			{ parseInt(this.props.error_city) === 1 ? <Alerta  error={'Oops!! Esta ciudad no esta disponible'} />  : ''   }
               			{ this.props.wheatherCity && typeof this.props.wheatherCity !== "undefined" && typeof this.props.wheatherCity === "object"  && Object.keys(this.props.wheatherCity).length > 0   ?  
               			<WheatherCity wheatherCity={this.props.wheatherCity}  />  : ''   
               			}
               			
					</div>
               </div>
            );
    }
}

const mapStateToProps=(state) =>{
    return {wheatherCity:state.wheatherCity , error_city:state.error_city }
};

export default connect (mapStateToProps, {loadCurrentWheater})(CurrentWheatherCity);