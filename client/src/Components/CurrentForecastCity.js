import React , {Component  } from 'react';
import {connect} from "react-redux";
import {loadForecastWheater } from "../Actions/index.js"
import Forecast from './Forecast';

class CurrentForecastCity  extends Component {
        
      constructor (props) {
        super(props)
      	this.props.loadForecastWheater()
      	
      }

        render() {
            
            return (
               <div className="row">
                 		<div className="col-md-12">
                    <div className="mb-1">
                        { this.props.wheatherForecast && typeof this.props.wheatherForecast !== "undefined" && typeof this.props.wheatherForecast === "object"  && Object.keys(this.props.wheatherForecast).length > 0 ?
                     			 <Forecast forecastCity={this.props.wheatherForecast} />

                            : ''
                        }
                     </div>
  					         </div>

               </div>
            );
    }
}

const mapStateToProps=(state) =>{
   return {wheatherForecast:state.wheatherForecast}
};

export default connect (mapStateToProps, { loadForecastWheater   })(CurrentForecastCity);