import React, { Component , Fragment } from 'react';
import Header from './Components/layout/Header';
import Footer from './Components/layout/Footer';
import CurrentWheatherCity from './Components/CurrentWheatherCity';
import CurrentForecastCity from './Components/CurrentForecastCity';

class App extends Component {
  render() {
    return (
       <Fragment>
       	    <div className="container">
	       		<Header />
	       		<CurrentWheatherCity />
	       		<CurrentForecastCity />
	       		<Footer/>
       		</div>
       </Fragment>
    );
  }
}

export default App;
